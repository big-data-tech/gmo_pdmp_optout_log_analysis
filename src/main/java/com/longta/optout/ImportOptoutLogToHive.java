package com.longta.optout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import com.longta.optout.OptoutLogAnalysis.OptoutLogDt;

public class ImportOptoutLogToHive {

	// Store log time
	public static class OptoutLogDt {
		public Long getLogTime() {
			return logTime;
		}

		public void setLogTime(Long logTime) {
			this.logTime = logTime;
		}

		public int getOptout() {
			return optout;
		}

		public void setOptout(int optout) {
			this.optout = optout;
		}

		private Long logTime = null;
		private int optout = 0;
	}

	public static class TokenizerMapper extends
			Mapper<Object, Text, Text, Text> {

		// Format date time of log
		private SimpleDateFormat format = new SimpleDateFormat(
				"dd/MMM/yyyy:HH:mm:ssZ", Locale.ENGLISH);
		private Text word = new Text();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String line = value.toString();
			String[] array = line.split(" ");
			String url = array[6]; // request URL
			String[] pairs = url.split("\\?");
			Map<String, String> parsedURL = splitQuery(pairs[1]);

			// get log time: [29/May/2014:15:56:42 +0900] =>
			// 29/May/2014:15:56:42+0900
			String rstTime = array[3].substring(1)
					+ array[4].substring(0, array[4].length() - 1);
			Date logTimeDate = null;
			try {
				logTimeDate = format.parse(rstTime);
				// timeDate = new Timestamp(.getTime());

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String valStr = parsedURL.get("s") + "\t" + logTimeDate.getTime();

			word.set(parsedURL.get("id"));
			Text val = new Text(valStr);

			context.write(word, val);
		}
	}

	public static class IntSumReducer extends Reducer<Text, Text, Text, Text> {

		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			Long optoutTimeOut = new Long("0");
			int optoutOut = 0;
			for (Text val : values) {
				String[] valArr = val.toString().split("\t");
				if (valArr.length == 2) {
					int optout = Integer.parseInt(valArr[0]);
					Long optoutTime = Long.parseLong(valArr[1]);
					boolean compare = optoutTime > optoutTimeOut ? true : false;
					if (compare) {
						optoutOut = optout;
						optoutTimeOut = optoutTime;
					}
				}

			}
			String valStr = new String(optoutOut + "\t" + optoutTimeOut);
			context.write(key, new Text(valStr));
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		if (otherArgs.length != 2) {
			System.err.println("Usage: ImportOptoutLogToHive <in> <out>");
			System.exit(2);
		}
		FileSystem fs = FileSystem.get(conf);
		Path input = new Path(otherArgs[0]);
		Path des = new Path(otherArgs[1]);
		Path output = new Path("/tmp/longta/log");
		if (fs.exists(output)) {
			fs.delete(output, true);
		}
		Job job = new Job(conf, "word count");
		job.setJarByClass(ImportOptoutLogToHive.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, output);
		int exit = job.waitForCompletion(true) ? 0 : 1;
		System.out.println("JOB DONE");

		FileStatus[] status = fs.listStatus(output);
		for (int i = 0; i < status.length; i++) {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(status[i].getPath())));
			String line;
			line = br.readLine();
			while (line != null) {
				System.out.println(line);
				line = br.readLine();
			}
			if (status[i].getLen() == 0) {
				fs.delete(status[i].getPath(), true);

			}

		}
		
		if (fs.exists(des)) {
			fs.delete(des, true);
		}
		fs.rename(output, des);
		System.exit(exit);

	}

	// Func tools
	/**
	 * parser URL request
	 * 
	 * @param query
	 * @return parsed Map<String, String>
	 * @throws UnsupportedEncodingException
	 */
	public static Map<String, String> splitQuery(String query)
			throws UnsupportedEncodingException {
		Map<String, String> query_pairs = new HashMap<String, String>();
		String[] pairs = query.split("&");
		for (int i = 0; i < pairs.length; i++) {
			String pair = pairs[i];
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return query_pairs;
	}
}
