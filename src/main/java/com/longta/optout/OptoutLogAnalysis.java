package com.longta.optout;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.net.ntp.TimeStamp;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.NLineInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class OptoutLogAnalysis extends Configured implements Tool {

	// Store log time
	public static class OptoutLogDt {
		public Long getLogTime() {
			return logTime;
		}

		public void setLogTime(Long logTime) {
			this.logTime = logTime;
		}

		public int getOptout() {
			return optout;
		}

		public void setOptout(int optout) {
			this.optout = optout;
		}

		private Long logTime = null;
		private int optout = 0;
	}

	public static class TokenizerMapper extends
			Mapper<Object, Text, Text, LongWritable> {

		// Format date time of log
		private SimpleDateFormat format = new SimpleDateFormat(
				"dd/MMM/yyyy:HH:mm:ssZ", Locale.ENGLISH);

		/**
		 * @param date
		 * @param timeLong
		 * @return
		 * @throws ParseException
		 */
		public boolean compareDate(String date, String timeLong)
				throws ParseException {

			// SimpleDateFormat format = new
			// SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z");

			Timestamp timestampLong = new Timestamp(Long.parseLong(timeLong));

			Timestamp timeDate = new Timestamp(format.parse(date).getTime());

			return timeDate.after(timestampLong);
		}

		/**
		 * @param optoutLogMap
		 * @param puts
		 * @param results
		 */
		public void getListPuts(Map<String, OptoutLogDt> optoutLogMap,
				List<Put> puts, Result[] results) {
			for (Result result : results) {
				int optout = 0;
				int optoutLogVl = 0;
				boolean flag = false;
				byte[] rowKey = null;
				Long timeLog = null;
				for (KeyValue kv : result.raw()) {
					System.out.println("Row is" + " "
							+ Bytes.toString(kv.getRow()));
					System.out.println("Family is" + " "
							+ Bytes.toString(kv.getFamily()));
					System.out.println("Qualifir is " + " "
							+ Bytes.toString(kv.getQualifier()));
					System.out.println("Value is" + " "
							+ Bytes.toString(kv.getValue()));
					System.out
							.println("----------------------------------------------------");
					if (Bytes.toString(kv.getQualifier()).compareTo(
							"output_time") == 0) {
						Long dbLogTime = new Long(Bytes.toString(kv.getValue()));
						rowKey = kv.getRow();
						optoutLogVl = optoutLogMap.get(Bytes.toString(rowKey)).getOptout();
						timeLog = optoutLogMap.get(Bytes.toString(rowKey))
								.getLogTime();
						flag = (timeLog > dbLogTime) ? true : false;
//						flag = true;

					} else if (Bytes.toString(kv.getQualifier()).compareTo(
							"optout") == 0) {
						optout = Integer.parseInt(Bytes.toString(kv.getValue()));
					}
				}
				if (flag) {
					if (optoutLogVl != optout) {
						Put put = new Put(rowKey);
						put.add(Bytes.toBytes("optout"),
								Bytes.toBytes("optout"), Bytes.toBytes(optoutLogVl + ""));
						puts.add(put);
						
						Put put2 = new Put(rowKey);
						put2.add(Bytes.toBytes("optout"),
								Bytes.toBytes("output_time"), Bytes.toBytes(timeLog.toString()));
						puts.add(put2);
					}
				}
			}

		}

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String lines = value.toString();
			String[] lineArr = lines.split("\n");
			int lcount = lineArr.length;
			Map<String, OptoutLogDt> optoutLogMap = new HashMap<String, OptoutLogDt>();
			for (int i = 0; i < lcount; i++) {

				String[] array = lineArr[i].split(" ");
				String url = array[6]; // request URL
				String[] pairs = url.split("\\?");
				Map<String, String> parsed_url = splitQuery(pairs[1]);

				// get log time: [29/May/2014:15:56:42 +0900] =>
				// 29/May/2014:15:56:42+0900
				String rstTime = array[3].substring(1)
						+ array[4].substring(0, array[4].length() - 1);
				// Timestamp timeDate = null;
				Date logTimeDate = null;
				try {
					logTimeDate = format.parse(rstTime);
					// timeDate = new Timestamp(.getTime());

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				OptoutLogDt logDt = optoutLogMap.get(lineArr[i]);
				// Long logLineMap = logDt.getLogTime();
				if (logDt == null) {
					OptoutLogDt newLogDt = new OptoutLogDt();
					System.err.println("MAPPPP. " + logTimeDate.getTime());
					newLogDt.setLogTime(logTimeDate.getTime());

					newLogDt.setOptout(Integer.parseInt(parsed_url.get("s")));
					// Put to map
					optoutLogMap.put(parsed_url.get("id"), newLogDt);
				} else {
					// Compare time
					// Long mapTime = optoutLogMap.get(key);
					Long logLineMap = logDt.getLogTime();
					boolean compareTime = false;
					try {
						compareTime = compareDate(logTimeDate.toString(),
								logLineMap.toString());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (compareTime) {
						OptoutLogDt newLogDt = new OptoutLogDt();
						newLogDt.setLogTime(logTimeDate.getTime());
						newLogDt.setOptout(Integer.parseInt(parsed_url.get("s")));
						optoutLogMap.put(parsed_url.get("id"), newLogDt);
					}

				}
			}
			List<Get> arrayGet = new ArrayList<Get>();

			Configuration conf = HBaseConfiguration.create();
			conf.set("hbase.zookeeper.quorum",
					"vm27801072,vm27801073,vm27801074");
			HConnection connection = HConnectionManager.createConnection(conf);
			HTableInterface table = connection.getTable("hb_user_cookie");

			// Check with hbase db
			for (String k : optoutLogMap.keySet()) {
				Get get = new Get(Bytes.toBytes(k));
				get.addFamily(Bytes.toBytes("optout"));
				arrayGet.add(get);
				 
			}

			// Get hbase db to check time
			Result[] r = table.get(arrayGet);

			List<Put> arrayPut = new ArrayList<Put>();
			
			getListPuts(optoutLogMap, arrayPut, r);
			
			table.put(arrayPut);
			context.write(new Text("LONGTA"),new
					 LongWritable(arrayPut.size()));

			table.close();
			connection.close();
		}
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new OptoutLogAnalysis(),
				args);
		System.out.println("HBASE");
		System.exit(res);

	}

	public static Map<String, String> splitQuery(String query)
			throws UnsupportedEncodingException {
		Map<String, String> query_pairs = new HashMap<String, String>();
		String[] pairs = query.split("&");
		for (int i = 0; i < pairs.length; i++) {
			String pair = pairs[i];
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return query_pairs;
	}

	public static int checkHbaseOptout(String id, String time, int optout)
			throws IOException {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.zookeeper.quorum", "vm27801072,vm27801073,vm27801074");
		HConnection connection = HConnectionManager.createConnection(conf);
		HTableInterface table = connection.getTable("test");
		Get get1 = new Get(Bytes.toBytes("id"));

		table.get(get1);

		return 1;
	}

	public final int run(final String[] args) throws Exception {
		// TODO Auto-generated method stub
		Configuration conf = super.getConf();
		// String[] otherArgs = new GenericOptionsParser(conf,
		// args).getRemainingArgs();
		// if (otherArgs.length != 2) {
		// System.err.println("Usage: wordcount <in> <out>");
		// System.exit(2);
		// }
		Job job = new Job(conf);
		String input = conf.get("input");
		String output = conf.get("output");
		// System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"+input);
		job.setJarByClass(OptoutLogAnalysis.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		job.setNumReduceTasks(0);
		job.setInputFormatClass(CustomFileInputFormat.class);

		FileInputFormat.addInputPath(job, new Path(input));
		FileOutputFormat.setOutputPath(job, new Path(output));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
		return 0;
	}

}
