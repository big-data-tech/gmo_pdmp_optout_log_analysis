package com.longta.optout;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class MapTest extends Mapper<Object, Text, Text, IntWritable> {

	private final static IntWritable one = new IntWritable(1);
	private Text word = new Text();

	public void map(Object key, Text value, Context context)
			throws IOException, InterruptedException {

		System.err.println("VALUE:" + value.toString());

//		Configuration conf = HBaseConfiguration.create();
//		conf.set("hbase.zookeeper.quorum", "vm27801072,vm27801073,vm27801074");
//		HConnection connection = HConnectionManager.createConnection(conf);
//		HTableInterface table = connection.getTable("hb_user_cookie");
//		Put put = new Put(Bytes.toBytes("cookie0001"));
//		put.add(Bytes.toBytes("ext_system_ids"), Bytes.toBytes("qua01"),
//				Bytes.toBytes("val01"));
//
//		table.put(put);
//		// use table as needed, the table returned is lightweight
//		table.close();
//		// use the connection for other access to the cluster
//		connection.close();

		word.set("longongo");
		context.write(word, one);
		//

	}
}