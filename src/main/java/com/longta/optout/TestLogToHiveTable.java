package com.longta.optout;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TestLogToHiveTable {

	public static class TokenizerMapper extends
			Mapper<Object, Text, Text, Text> {

		private Text word = new Text();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String line = value.toString();
			String[] array = line.trim().replaceAll("	", " ").split(" ");
			String url = array[5]; // request URL
			String[] pairs = url.split("\\?");
			if (pairs.length == 2) {
				Map<String, String> parsedURL = splitQuery(pairs[1]);
				if (parsedURL.get("froms") != null
						&& parsedURL.get("to") != null) {
					word.set(parsedURL.get("from"));
					Text val = new Text(parsedURL.get("to"));
					context.write(word, val);
				}
			}
		}
	}

	private static String LOG_PATH = new String("/tmp/logs/");
	private static String ANALYSIS_PATH = new String("/tmp/relation/analysis");
	private static String HIVE_TABLE_PATH = new String("/tmp/relation/des/");

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		// if (otherArgs.length != 2) {
		// System.err.println("Usage: ImportOptoutLogToHive <in> <out>");
		// System.exit(2);
		// }
		Job job = new Job(conf, "word count");
		job.setJarByClass(ImportOptoutLogToHive.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setNumReduceTasks(0);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.addInputPath(job, new Path(ANALYSIS_PATH));
		FileOutputFormat.setOutputPath(job, new Path(HIVE_TABLE_PATH));
		int exit = job.waitForCompletion(true) ? 0 : 1;
		System.out.println("JOB DONE");
		System.exit(exit);
	}

	/**
	 * parser URL request
	 * 
	 * @param query
	 * @return parsed Map<String, String>
	 * @throws UnsupportedEncodingException
	 */
	public static Map<String, String> splitQuery(String query)
			throws UnsupportedEncodingException {
		Map<String, String> query_pairs = new HashMap<String, String>();
		String[] pairs = query.split("&");
		for (int i = 0; i < pairs.length; i++) {
			String pair = pairs[i];
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return query_pairs;
	}
}
