package com.longta.optout;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MapReduceExample extends Configured implements Tool {

	public static class TokenizerMapper extends
			Mapper<Object, Text, Text, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {

			String val = value.toString();
//			String[] array = val.split(" ");
//			String url = array[6];
//
//			String[] pairs = url.split("\\?");
//			Map<String, String> parsed_url = splitQuery(pairs[1]);
//			String cookieID = parsed_url.get("id").toString();
//			String optoutStatus = parsed_url.get("id").toString();
//			System.err.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
//			word.set(cookieID);
			// checkHbaseOptout(cookieID,"902389",1);
			Configuration conf = HBaseConfiguration.create();
			conf.set("hbase.zookeeper.quorum",
					"vm27801072,vm27801073,vm27801074");
			HConnection connection = HConnectionManager.createConnection(conf);
	    	HTableInterface table = connection.getTable("hb_user_cookie");
	    	Put put = new Put(Bytes.toBytes("cookie0001"));
	    	put.add(Bytes.toBytes("ext_system_ids"),Bytes.toBytes("qua01"),Bytes.toBytes("val01"));
	    	
	    	table.put(put);
	    	// use table as needed, the table returned is lightweight
	    	table.close();
	    	// use the connection for other access to the cluster
	    	connection.close();

			// table.get(get1);
			// word.set(pairs[1]);
			// context.write(word, one);
			// StringTokenizer itr = new StringTokenizer(value.toString());
			// while (itr.hasMoreTokens()) {
			// word.set(itr.nextToken());
			// context.write(word, one);
			// }
		}
	}

	public int run(String[] args) throws Exception {
		Job job = new Job(super.getConf());
		job.setMapperClass(TokenizerMapper.class);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
		return 0;
	}

	public static void main(String[] args) throws Exception {
		// FileUtils.deleteDirectory(new File("data/output"));
		// args = new String[] { "data/input", "data/output" };
		ToolRunner.run(new MapReduceExample(), args);
	}
}
