//package com.longta.optout;
//
//import java.io.IOException;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.conf.Configured;
//import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.IntWritable;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapred.JobClient;
//import org.apache.hadoop.mapred.JobConf;
//import org.apache.hadoop.mapred.MapReduceBase;
//import org.apache.hadoop.mapred.Mapper;
//import org.apache.hadoop.mapred.OutputCollector;
//import org.apache.hadoop.mapred.Reporter;
//import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.Mapper.Context;
//import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
//import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
//import org.apache.hadoop.util.Tool;
//import org.apache.hadoop.util.ToolRunner;
//
//public class MRTest extends Configured implements Tool {
//
//	static class TestMapper extends MapReduceBase implements
//			Mapper<Object, Text, Text, IntWritable> {
//
//		// LongWritable ONE = new LongWritable(1L);
//		public void map(Object key, Text value,
//				OutputCollector<Text, IntWritable> output, Reporter reporter)
//				throws IOException {
//			// TODO Auto-generated method stub
//
//		}
//	}
//
//	public final int run(final String[] args) throws Exception {
//
//		Configuration conf = super.getConf();
//		// Job job = new Job(conf);
//		// String input = conf.get("input");
//		// String output = conf.get("output");
//		// job.setJarByClass(MRTest.class);
//		//
//		// job.setMapperClass(MapTest.class);
//		// job.setNumReduceTasks(0);
//		// job.setOutputKeyClass(Text.class);
//		// job.setOutputValueClass(IntWritable.class);
//		// FileInputFormat.addInputPath(job, new Path(input));
//		// FileOutputFormat.setOutputPath(job, new Path(output));
//		// JobClient.runJob(job);
//		// job.waitForCompletion(true);
//		JobConf job = new JobConf(getConf(), MRTest.class);
//
//		job.setJobName("Count Pageviews of URLs");
//
//		job.setMapperClass(TestMapper.class);
//		job.setNumReduceTasks(0);
//
//		job.setMapOutputKeyClass(Text.class);
//		job.setMapOutputValueClass(IntWritable.class);
//
//		job.setOutputKeyClass(Text.class);
//		job.setOutputValueClass(IntWritable.class);
//		String input = conf.get("input");
//
//		String output = conf.get("output");
//	    FileInputFormat.addInputPaths(job, new Path(input));
//	    FileOutputFormat.setOutputPath(job, new Path(output));
//		JobClient.runJob(job);
//
//		return 0;
//	}
//
//	public static void main(final String[] args) throws Exception {
//		Configuration conf = new Configuration();
//		int res = ToolRunner.run(conf, new MRTest(), args);
//		System.exit(res);
//	}
//}
